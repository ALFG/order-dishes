package com.alcohol.orderDishes.controller;

import com.alcohol.orderDishes.FileTools;
import com.alcohol.orderDishes.entity.MenuInfo;
import com.alcohol.orderDishes.service.MenuService;
import com.alcohol.orderDishes.vo.request.MenuInfoVO;
import com.alcohol.orderDishes.vo.response.HttpResponseVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Author: Lixinyu
 * @Date: Created in 14:54 2019-02-26
 * @Description:
 * @Modified By:
 */
@Slf4j
@RestController
@RequestMapping("/menu/v1")
public class MenuController {

    private static final String STATIC = "static/";
    private static final String POINT = ".";
    private static final String URL = "http://localhost:8888/";

    @Resource
    private MenuService menuService;

    @GetMapping({"/getAllMenus/{type}", "/getAllMenus"})
    public HttpResponseVO getAllMenus(@PathVariable(required = false) Integer type) {
        List<MenuInfo> result;
        try {
            result = menuService.getAllMenus(type);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("查询全部菜谱失败");
        }

        return HttpResponseVO.ok("查询全部菜谱成功", result);
    }

    @GetMapping("/getMenuDetail/{id}")
    public HttpResponseVO getMenuDetail(@PathVariable Long id) {
        MenuInfo result;
        try {
            result = menuService.getMenuDetail(id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("查询菜谱失败");
        }

        return HttpResponseVO.ok("查询菜谱成功", result);
    }

    @DeleteMapping("/deleteMenu/{id}")
    public HttpResponseVO deleteMenu(@NonNull @PathVariable("id") Integer id) {
        try {
            menuService.deleteMenu(id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("删除菜谱失败");
        }

        return HttpResponseVO.ok("删除菜谱成功");
    }

    @PostMapping("/addMenu")
    public HttpResponseVO addMenu(@NonNull @RequestBody MenuInfoVO menuInfoVO) {
        try {
            menuService.addMenu(menuInfoVO);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("添加菜谱失败");
        }

        return HttpResponseVO.ok("添加菜谱成功");
    }

    @PutMapping("/editMenu")
    public HttpResponseVO editMenu(@NonNull @RequestBody MenuInfoVO menuInfoVO) {
        try {
            menuService.editMenu(menuInfoVO);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("编辑菜谱失败");
        }

        return HttpResponseVO.ok("编辑菜谱成功");
    }

    @PostMapping("/uploadPicture")
    public HttpResponseVO EditMenu(@NonNull @RequestParam("file") MultipartFile file) {
        String fileName ;
        try {
            fileName = file.getOriginalFilename();
            assert fileName != null;
            String type = fileName.substring(fileName.indexOf(POINT));
            fileName = System.currentTimeMillis() + type;

            byte[] in = file.getBytes();
            ClassLoader classLoader = ClassUtils.getDefaultClassLoader();
            Objects.requireNonNull(classLoader);
            String uriStr = Objects.requireNonNull(classLoader.getResource("")).getPath() + STATIC;
            FileTools.uploadFile(in, uriStr, fileName);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("上传图片失败");
        }

        return HttpResponseVO.ok("上传图片成功", URL + fileName);
    }
}