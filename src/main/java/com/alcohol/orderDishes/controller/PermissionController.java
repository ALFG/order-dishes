package com.alcohol.orderDishes.controller;

import com.alcohol.orderDishes.entity.PermissionInfo;
import com.alcohol.orderDishes.entity.RoleInfo;
import com.alcohol.orderDishes.service.PermissionService;
import com.alcohol.orderDishes.vo.request.MenuInfoVO;
import com.alcohol.orderDishes.vo.response.HttpResponseVO;
import com.alcohol.orderDishes.vo.response.RoleInfoVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Lixinyu
 * @Date: Created in 10:29 2019-02-27
 * @Description:
 * @Modified By:
 */
@Slf4j
@RestController
@RequestMapping("/permission/v1")
public class PermissionController {

    @Resource
    private PermissionService permissionService;

    @GetMapping("/getAllPermission/{openId}")
    public HttpResponseVO getAllPermission(@PathVariable("openId") String openId) {
        List<PermissionInfo> result;
        try {
            result = permissionService.getAllPermission(openId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("查询权限失败");
        }

        return HttpResponseVO.ok("查询权限成功", result);
    }

    @GetMapping("/getRoleInfo/{openId}")
    public HttpResponseVO getRoleInfo(@NonNull @PathVariable("openId") String openId) {
        List<RoleInfo> result;
        try {
            result = permissionService.getRoleInfo(openId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("查询角色失败");
        }

        return HttpResponseVO.ok("查询角色成功", result);
    }

    @GetMapping("/getAllRoleInfo")
    public HttpResponseVO getAllRoleInfo() {
        List<RoleInfoVO> result;
        try {
            result = permissionService.getAllRoleInfo();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("查询角色失败");
        }

        return HttpResponseVO.ok("查询角色成功", result);
    }

    @PostMapping("/editRole")
    public HttpResponseVO editRole(@NonNull @RequestBody RoleInfoVO roleInfoVO) {
        try {
            permissionService.editRole(roleInfoVO);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("编辑角色失败");
        }

        return HttpResponseVO.ok("编辑角色成功");
    }
}