package com.alcohol.orderDishes.controller;

import com.alcohol.orderDishes.service.WechatService;
import com.alcohol.orderDishes.vo.request.LoginVO;
import com.alcohol.orderDishes.vo.response.HttpResponseVO;
import com.alibaba.fastjson.JSONObject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @Author: Lixinyu
 * @Date: Created in 10:49 2019-02-28
 * @Description:
 * @Modified By:
 */
@Slf4j
@RestController
@RequestMapping("/wechat/v1")
public class WechatController {

    @Resource
    private WechatService wechatService;

    @PostMapping("/login")
    public HttpResponseVO login(@NonNull @RequestBody LoginVO loginVO) {
        JSONObject result;
        try {
            String appId = loginVO.getAppId();
            Objects.requireNonNull(appId, "appId为空");

            String appSecret = loginVO.getAppSecret();
            Objects.requireNonNull(appSecret, "appSecret为空");

            String code = loginVO.getCode();
            Objects.requireNonNull(code, "code为空");

            result = wechatService.login(appId, appSecret, code);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("登录失败");
        }

        return HttpResponseVO.ok("登录成功", result);
    }
}
