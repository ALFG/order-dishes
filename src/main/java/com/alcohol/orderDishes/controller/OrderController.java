package com.alcohol.orderDishes.controller;

import com.alcohol.orderDishes.entity.OrderInfo;
import com.alcohol.orderDishes.service.OrderService;
import com.alcohol.orderDishes.vo.request.*;
import com.alcohol.orderDishes.vo.response.AllOrderDetailInfoVO;
import com.alcohol.orderDishes.vo.response.HttpResponseVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Author: Lixinyu
 * @Date: Created in 10:29 2019-02-27
 * @Description:
 * @Modified By:
 */
@Slf4j
@RestController
@RequestMapping("/order/v1")
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("/getAllOrder/{status}")
    public HttpResponseVO getAllMenus(@PathVariable("status") Integer status) {
        List<OrderInfo> result;
        try {
            result = orderService.getAllOrder(status);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("查询订单失败");
        }

        return HttpResponseVO.ok("查询订单成功", result);
    }

    @GetMapping("/getAllOrderDetail/{tableId}/{openId}")
    public HttpResponseVO getAllOrderDetail(@NonNull @PathVariable("tableId") Long tableId,
                                            @NonNull @PathVariable("openId") String openId) {
        AllOrderDetailInfoVO result;
        try {
            result = orderService.getAllOrderDetail(tableId, openId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("查询订单详情失败");
        }

        return HttpResponseVO.ok("查询订单详情成功", result);
    }

    @PostMapping("/addOrderDetail")
    public HttpResponseVO addOrderDetail(@NonNull @RequestBody AddOrderDetailVO addOrderDetailVO) {
        try {
            Long tableId = addOrderDetailVO.getTableId();
            Objects.requireNonNull(tableId, "餐桌号为空");

            String openId = addOrderDetailVO.getOpenId();
            Objects.requireNonNull(openId, "当前用户为空");

            Long menuId = addOrderDetailVO.getMenuId();
            Objects.requireNonNull(menuId, "菜单为空");

            orderService.addOrderDetail(tableId, openId, menuId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("添加订单详情失败");
        }

        return HttpResponseVO.ok("添加订单详情成功");
    }

    @PutMapping("/placeAnOrder")
    public HttpResponseVO placeAnOrder(@NonNull @RequestBody List<PlaceAnOrderVO> placeAnOrderVOList) {
        for (PlaceAnOrderVO placeAnOrderVO : placeAnOrderVOList) {
            try {
                Long orderDetailId = placeAnOrderVO.getId();
                Objects.requireNonNull(orderDetailId, "订单详情ID为空");

                Integer num = placeAnOrderVO.getNum();
                Objects.requireNonNull(num, "数量为空");

                orderService.placeAnOrder(orderDetailId, num);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return HttpResponseVO.error("下单失败");
            }
        }

        return HttpResponseVO.ok("下单成功");
    }

    @PutMapping("/checkout")
    public HttpResponseVO checkout(@NonNull @RequestBody CheckoutVO checkoutVO) {
        try {

            Long orderInfoId = checkoutVO.getOrderInfoId();
            Objects.requireNonNull(orderInfoId, "订单ID为空");

            orderService.checkout(orderInfoId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("支付订单失败");
        }

        return HttpResponseVO.ok("支付订单成功");
    }

    @PutMapping("/updateOrderStatus")
    public HttpResponseVO updateOrderStatus(@NonNull @RequestBody UpdateOrderStatusVO updateOrderStatusVO) {
        try {
            Long orderInfoId = updateOrderStatusVO.getOrderInfoId();
            Objects.requireNonNull(orderInfoId, "订单ID为空");

            Integer status = updateOrderStatusVO.getStatus();
            Objects.requireNonNull(status, "状态为空");

            orderService.updateOrderStatus(orderInfoId, status);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("修改订单状态失败");
        }

        return HttpResponseVO.ok("修改订单状态成功");
    }

    @PutMapping("/callService")
    public HttpResponseVO callService(@NonNull @RequestBody CallServiceVO callServiceVO) {
        try {
            String openId = callServiceVO.getOpenId();
            Objects.requireNonNull(openId, "用户ID为空");

            Long tableId = callServiceVO.getTableId();
            Objects.requireNonNull(tableId, "餐桌号为空");

            orderService.callService(tableId, openId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("呼叫失败");
        }

        return HttpResponseVO.ok("呼叫成功");
    }

    @PutMapping("/callServiceFinish")
    public HttpResponseVO callServiceFinish(@NonNull @RequestBody CallServiceFinishVO callServiceFinishVO) {
        try {
            Long orderInfoId = callServiceFinishVO.getOrderInfoId();
            Objects.requireNonNull(orderInfoId, "订单ID为空");

            orderService.callServiceFinish(orderInfoId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("服务完成失败");
        }

        return HttpResponseVO.ok("服务完成成功");
    }

    @PutMapping("/turnTable")
    public HttpResponseVO turnTable(@NonNull @RequestBody TurnTableVO turnTableVO) {
        try {
            Long orderInfoId = turnTableVO.getOrderInfoId();
            Objects.requireNonNull(orderInfoId, "订单ID为空");

            Long tableId = turnTableVO.getTableId();
            Objects.requireNonNull(orderInfoId, "餐桌ID为空");

            orderService.turnTable(orderInfoId, tableId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return HttpResponseVO.error("转台失败");
        }

        return HttpResponseVO.ok("转台成功");
    }
}