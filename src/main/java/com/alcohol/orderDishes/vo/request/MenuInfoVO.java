package com.alcohol.orderDishes.vo.request;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: Lixinyu
 * @Date: Created in 16:48 2019-02-26
 * @Description:
 * @Modified By:
 */
@Data
public class MenuInfoVO {

    private Long id;
    private String pictureUrl;
    private String title;
    private String desc;
    private BigDecimal price;
    private Integer type;
}