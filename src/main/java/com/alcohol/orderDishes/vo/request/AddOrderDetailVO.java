package com.alcohol.orderDishes.vo.request;

import lombok.Data;

/**
 * @Author: Lixinyu
 * @Date: Created in 14:33 2019-02-27
 * @Description:
 * @Modified By:
 */
@Data
public class AddOrderDetailVO {

    private Long tableId;
    private String openId;
    private Long menuId;
}