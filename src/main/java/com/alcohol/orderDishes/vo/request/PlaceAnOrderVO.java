package com.alcohol.orderDishes.vo.request;

import lombok.Data;

/**
 * @Author: Lixinyu
 * @Date: Created in 14:25 2019-02-27
 * @Description:
 * @Modified By:
 */
@Data
public class PlaceAnOrderVO {

    private Long id;
    private Integer num;
}