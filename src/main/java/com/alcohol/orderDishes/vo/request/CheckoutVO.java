package com.alcohol.orderDishes.vo.request;

import lombok.Data;

/**
 * @Author: Lixinyu
 * @Date: Created in 15:33 2019-02-27
 * @Description:
 * @Modified By:
 */
@Data
public class CheckoutVO {

    private Long orderInfoId;
}