package com.alcohol.orderDishes.vo.request;

import lombok.Data;

/**
 * @Author: Lixinyu
 * @Date: Created in 10:53 2019-02-28
 * @Description:
 * @Modified By:
 */
@Data
public class LoginVO {

    private String appId;
    private String appSecret;
    private String code;
}