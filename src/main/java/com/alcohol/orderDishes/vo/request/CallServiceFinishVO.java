package com.alcohol.orderDishes.vo.request;

import lombok.Data;

/**
 * @Author: Lixinyu
 * @Date: Created in 00:32 2019-03-09
 * @Description:
 * @Modified By:
 */
@Data
public class CallServiceFinishVO {

    private Long orderInfoId;
}