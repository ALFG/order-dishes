package com.alcohol.orderDishes.vo.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author: Lixinyu
 * @Date: Created in 21:41 2019-03-08
 * @Description:
 * @Modified By:
 */
@Builder
@ToString
@Getter
public class PermissionInfoVO {

    private Long id;
    private String permissionName;
    @Setter
    private Boolean checked;
}