package com.alcohol.orderDishes.vo.response;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

/**
 * @Author: Lixinyu
 * @Date: Created in 16:38 2019-02-26
 * @Description:
 * @Modified By:
 */
@Builder
@ToString
@Getter
public class HttpResponseVO {

    private Integer status;
    private String msg;
    private Object data;

    public static HttpResponseVO ok(String msg, Object data) {
        return new HttpResponseVO(HttpStatus.OK.value(), msg, data);
    }

    public static HttpResponseVO ok(String msg) {
        return new HttpResponseVO(HttpStatus.OK.value(), msg, null);
    }

    public static HttpResponseVO error(String msg, Object data) {
        return new HttpResponseVO(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, data);
    }

    public static HttpResponseVO error(String msg) {
        return new HttpResponseVO(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, null);
    }
}