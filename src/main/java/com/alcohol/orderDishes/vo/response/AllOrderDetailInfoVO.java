package com.alcohol.orderDishes.vo.response;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author: Lixinyu
 * @Date: Created in 15:11 2019-02-27
 * @Description:
 * @Modified By:
 */
@Builder
@Getter
@ToString
public class AllOrderDetailInfoVO {

    private Long orderId;
    private BigDecimal total;
    private List<OrderDetailInfoVO> cartList;
    private List<OrderDetailInfoVO> orderList;
}