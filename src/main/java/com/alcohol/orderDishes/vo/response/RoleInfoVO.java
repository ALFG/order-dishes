package com.alcohol.orderDishes.vo.response;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

/**
 * @Author: Lixinyu
 * @Date: Created in 21:38 2019-03-08
 * @Description:
 * @Modified By:
 */
@Builder
@ToString
@Getter
public class RoleInfoVO {

    private long id;
    private String roleName;
    private List<PermissionInfoVO> permissionInfoVOList;
}