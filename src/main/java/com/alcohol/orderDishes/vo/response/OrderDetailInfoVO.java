package com.alcohol.orderDishes.vo.response;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * @Author: Lixinyu
 * @Date: Created in 11:25 2019-02-27
 * @Description:
 * @Modified By:
 */
@Builder
@Getter
@ToString
public class OrderDetailInfoVO {

    private Long orderDetailId;
    private String title;
    private String pictureUrl;
    private BigDecimal price;
    private Integer status;
    private Integer num;
    private Boolean selected;
}