package com.alcohol.orderDishes.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author alcohol
 * @since 2019-03-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OrderInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 修改时间
     */
    private LocalDateTime modifyDate;

    /**
     * 餐桌ID
     */
    private Long tableId;

    /**
     * 微信用户ID
     */
    private String openId;

    /**
     * 总价
     */
    private BigDecimal total;

    /**
     * 状态（0.待下单，1.待烹饪，2.待上菜，3.待结账，4.结账完成）
     */
    private Integer status;

    /**
     * 是否作废（0.是，1.否）
     */
    private Integer isDelete;

    /**
     * 呼叫标记
     */
    private Integer flag;


}
