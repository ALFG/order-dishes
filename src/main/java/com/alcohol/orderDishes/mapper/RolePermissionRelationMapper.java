package com.alcohol.orderDishes.mapper;

import com.alcohol.orderDishes.entity.RolePermissionRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author alcohol
 * @since 2019-02-28
 */
public interface RolePermissionRelationMapper extends BaseMapper<RolePermissionRelation> {

}
