package com.alcohol.orderDishes.mapper;

import com.alcohol.orderDishes.entity.OrderDetailInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author alcohol
 * @since 2019-02-27
 */
public interface OrderDetailInfoMapper extends BaseMapper<OrderDetailInfo> {

}
