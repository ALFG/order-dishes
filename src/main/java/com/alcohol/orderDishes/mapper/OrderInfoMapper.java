package com.alcohol.orderDishes.mapper;

import com.alcohol.orderDishes.entity.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author alcohol
 * @since 2019-03-05
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

    Long insertReturnKey(OrderInfo orderInfo);
}