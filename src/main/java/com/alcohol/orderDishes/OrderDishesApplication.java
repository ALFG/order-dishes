package com.alcohol.orderDishes;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.alcohol.orderDishes.mapper")
public class OrderDishesApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderDishesApplication.class, args);
    }

}
