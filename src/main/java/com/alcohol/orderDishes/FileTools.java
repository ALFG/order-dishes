package com.alcohol.orderDishes;

import lombok.Cleanup;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Author: Lixinyu
 * @Date: Created in 17:13 2019-02-26
 * @Description:
 * @Modified By:
 */
public interface FileTools {

    static void uploadFile(byte[] in, String fileDirUri, String fileName) throws IOException {
        File targetFile = new File(fileDirUri);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }

        @Cleanup
        FileOutputStream fileOutputStream = new FileOutputStream(fileDirUri + fileName);
        fileOutputStream.write(in);
    }
}
