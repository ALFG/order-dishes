package com.alcohol.orderDishes.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author: Lixinyu
 * @Date: Created in 13:42 2019-02-27
 * @Description:
 * @Modified By:
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum OrderDetailInfoStatus {

    ToPlaceTheOrder(0, "待下单"), HaveOrder(1, "已下单");

    private Integer code;
    private String desc;
}