package com.alcohol.orderDishes.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author: Lixinyu
 * @Date: Created in 15:47 2019-02-26
 * @Description:
 * @Modified By:
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum MenuType {

    HotDishes(0, "热菜"),
    ColdDishes(1, "冷菜"),
    Food(2, "主食");

    private Integer code;
    private String desc;
}
