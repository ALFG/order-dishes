package com.alcohol.orderDishes.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author: Lixinyu
 * @Date: Created in 01:04 2019-03-09
 * @Description:
 * @Modified By:
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Flag {

    Call(0, "呼叫"), Finish(1, "完成");

    private Integer code;
    private String desc;
}