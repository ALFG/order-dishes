package com.alcohol.orderDishes.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author: Lixinyu
 * @Date: Created in 13:32 2019-02-27
 * @Description:
 * @Modified By:
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum OrderInfoStatus {

    ToPlaceTheOrder(0, "待下单"),
    ToForCooking(1, "待烹饪"),
    ToBeServed(2, "待上菜"),
    ToCheckOut(3, "待结账"),
    CheckCompleted(4, "结账完成");

    private Integer code;
    private String desc;
}