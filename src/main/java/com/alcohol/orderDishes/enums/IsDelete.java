package com.alcohol.orderDishes.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author: Lixinyu
 * @Date: Created in 11:39 2019-02-27
 * @Description:
 * @Modified By:
 */

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum IsDelete {

    Yes(0, "是"), No(1, "否");

    private Integer code;
    private String desc;
}
