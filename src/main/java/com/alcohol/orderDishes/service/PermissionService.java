package com.alcohol.orderDishes.service;

import com.alcohol.orderDishes.entity.PermissionInfo;
import com.alcohol.orderDishes.entity.RoleInfo;
import com.alcohol.orderDishes.vo.response.RoleInfoVO;

import java.util.List;

/**
 * @Author: Lixinyu
 * @Date: Created in 11:12 2019-02-27
 * @Description:
 * @Modified By:
 */
public interface PermissionService {

    List<PermissionInfo> getAllPermission(String openId);
    List<RoleInfo> getRoleInfo(String openId);
    List<RoleInfoVO> getAllRoleInfo();
    void editRole(RoleInfoVO roleInfoVO);
}