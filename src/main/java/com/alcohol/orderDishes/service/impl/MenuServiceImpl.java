package com.alcohol.orderDishes.service.impl;

import com.alcohol.orderDishes.entity.MenuInfo;
import com.alcohol.orderDishes.mapper.MenuInfoMapper;
import com.alcohol.orderDishes.service.MenuService;
import com.alcohol.orderDishes.vo.request.MenuInfoVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Lixinyu
 * @Date: Created in 15:44 2019-02-26
 * @Description:
 * @Modified By:
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Resource
    private MenuInfoMapper menuInfoMapper;

    @Override
    public List<MenuInfo> getAllMenus(Integer type) {
        return menuInfoMapper.selectList(new QueryWrapper<MenuInfo>().eq(!StringUtils.isEmpty(type), "type", type));
    }

    @Override
    public MenuInfo getMenuDetail(Long id) {
        return menuInfoMapper.selectById(id);
    }

    @Override
    public void deleteMenu(Integer id) {
        menuInfoMapper.deleteById(id);
    }

    @Override
    public void addMenu(MenuInfoVO menuInfoVO) {
        MenuInfo menuInfo = new MenuInfo();
        BeanUtils.copyProperties(menuInfoVO, menuInfo);
        menuInfoMapper.insert(menuInfo);
    }

    @Override
    public void editMenu(MenuInfoVO menuInfoVO) {
        MenuInfo menuInfo = new MenuInfo();
        BeanUtils.copyProperties(menuInfoVO, menuInfo);
        menuInfoMapper.updateById(menuInfo);
    }
}
