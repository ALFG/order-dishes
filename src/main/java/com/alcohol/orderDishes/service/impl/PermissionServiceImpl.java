package com.alcohol.orderDishes.service.impl;

import com.alcohol.orderDishes.entity.PermissionInfo;
import com.alcohol.orderDishes.entity.RoleInfo;
import com.alcohol.orderDishes.entity.RolePermissionRelation;
import com.alcohol.orderDishes.entity.UserRoleRelation;
import com.alcohol.orderDishes.mapper.PermissionInfoMapper;
import com.alcohol.orderDishes.mapper.RoleInfoMapper;
import com.alcohol.orderDishes.mapper.RolePermissionRelationMapper;
import com.alcohol.orderDishes.mapper.UserRoleRelationMapper;
import com.alcohol.orderDishes.service.PermissionService;
import com.alcohol.orderDishes.vo.response.PermissionInfoVO;
import com.alcohol.orderDishes.vo.response.RoleInfoVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @Author: Lixinyu
 * @Date: Created in 09:51 2019-02-28
 * @Description:
 * @Modified By:
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    @Resource
    private UserRoleRelationMapper userRoleRelationMapper;
    @Resource
    private RolePermissionRelationMapper rolePermissionRelationMapper;
    @Resource
    private PermissionInfoMapper permissionInfoMapper;
    @Resource
    private RoleInfoMapper roleInfoMapper;

    @Override
    public List<PermissionInfo> getAllPermission(String openId) {
        List<UserRoleRelation> userRoleRelationList = this.getRoleIdByOpenId(openId);
        List<Long> permissionIdList = Lists.newArrayList();
        userRoleRelationList.forEach(userRoleRelation -> {
            Long roleId = userRoleRelation.getRoleInfoId();
            List<RolePermissionRelation> rolePermissionRelationList =
                    rolePermissionRelationMapper.selectList(new QueryWrapper<RolePermissionRelation>().eq("role_info_id", roleId));
            rolePermissionRelationList.forEach(rolePermissionRelation -> permissionIdList.add(rolePermissionRelation.getPermissionInfoId()));
        });

        return permissionInfoMapper.selectBatchIds(permissionIdList);
    }

    @Override
    public List<RoleInfo> getRoleInfo(String openId) {
        List<UserRoleRelation> userRoleRelationList = this.getRoleIdByOpenId(openId);
        if (CollectionUtils.isEmpty(userRoleRelationList)) {
            return null;
        }
        List<Long> roleIdList = Lists.newArrayList();
        userRoleRelationList.forEach(userRoleRelation -> roleIdList.add(userRoleRelation.getRoleInfoId()));

        return roleInfoMapper.selectBatchIds(roleIdList);
    }

    @Override
    public List<RoleInfoVO> getAllRoleInfo() {
        List<RoleInfoVO> result = Lists.newArrayList();

        List<RoleInfo> roleInfoList = roleInfoMapper.selectList(null);
        if (CollectionUtils.isEmpty(roleInfoList)) {
            return null;
        }



        roleInfoList.forEach(roleInfo -> {
            List<PermissionInfo> permissionInfoList = permissionInfoMapper.selectList(null);
            List<PermissionInfoVO> permissionInfoVOList = Lists.newArrayList();
            permissionInfoList.forEach(permissionInfo -> {
                permissionInfoVOList.add(PermissionInfoVO.builder().id(permissionInfo.getId())
                        .permissionName(permissionInfo.getPermissionName()).checked(Boolean.FALSE).build());
            });

            Long roleInfoId = roleInfo.getId();
            RoleInfoVO roleInfoVO = RoleInfoVO.builder().id(roleInfoId).roleName(roleInfo.getRoleName())
                    .permissionInfoVOList(permissionInfoVOList).build();

            List<RolePermissionRelation> rolePermissionRelationList =
                    rolePermissionRelationMapper.selectList(new QueryWrapper<RolePermissionRelation>().eq("role_info_id", roleInfoId));

            if (!CollectionUtils.isEmpty(permissionInfoVOList)) {
                permissionInfoVOList.forEach(permissionInfoVO -> {
                    Long permissionInfoId = permissionInfoVO.getId();
                    rolePermissionRelationList.forEach(rolePermissionRelation -> {
                        if (Objects.equals(permissionInfoId, rolePermissionRelation.getPermissionInfoId())) {
                            permissionInfoVO.setChecked(Boolean.TRUE);
                        }
                    });
                });
            }

            result.add(roleInfoVO);
        });

        return result;
    }

    @Override
    public void editRole(RoleInfoVO roleInfoVO) {
        List<PermissionInfoVO> permissionInfoVOList = roleInfoVO.getPermissionInfoVOList();
        Long roleInfoId = roleInfoVO.getId();
        rolePermissionRelationMapper.delete(new QueryWrapper<RolePermissionRelation>().eq("role_info_id", roleInfoId));

        if (CollectionUtils.isEmpty(permissionInfoVOList)) {
            return;
        }

        permissionInfoVOList.forEach(permissionInfoVO -> {
            if (permissionInfoVO.getChecked()) {
                RolePermissionRelation rolePermissionRelation = new RolePermissionRelation();
                rolePermissionRelation.setPermissionInfoId(permissionInfoVO.getId());
                rolePermissionRelation.setRoleInfoId(roleInfoId);
                rolePermissionRelationMapper.insert(rolePermissionRelation);
            }
        });
    }

    private List<UserRoleRelation> getRoleIdByOpenId(String openId) {
        return userRoleRelationMapper.selectList(new QueryWrapper<UserRoleRelation>().eq("open_id", openId));
    }
}