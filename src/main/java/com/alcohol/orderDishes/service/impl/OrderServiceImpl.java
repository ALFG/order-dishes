package com.alcohol.orderDishes.service.impl;

import com.alcohol.orderDishes.entity.MenuInfo;
import com.alcohol.orderDishes.entity.OrderDetailInfo;
import com.alcohol.orderDishes.entity.OrderInfo;
import com.alcohol.orderDishes.enums.Flag;
import com.alcohol.orderDishes.enums.IsDelete;
import com.alcohol.orderDishes.enums.OrderDetailInfoStatus;
import com.alcohol.orderDishes.enums.OrderInfoStatus;
import com.alcohol.orderDishes.mapper.MenuInfoMapper;
import com.alcohol.orderDishes.mapper.OrderDetailInfoMapper;
import com.alcohol.orderDishes.mapper.OrderInfoMapper;
import com.alcohol.orderDishes.service.OrderService;
import com.alcohol.orderDishes.vo.response.AllOrderDetailInfoVO;
import com.alcohol.orderDishes.vo.response.OrderDetailInfoVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @Author: Lixinyu
 * @Date: Created in 11:13 2019-02-27
 * @Description:
 * @Modified By:
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderInfoMapper orderInfoMapper;
    @Resource
    private OrderDetailInfoMapper orderDetailInfoMapper;
    @Resource
    private MenuInfoMapper menuInfoMapper;

    @Override
    public List<OrderInfo> getAllOrder(Integer status) {
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("status", status);
        paramMap.put("isDelete", IsDelete.No.getCode());
        return orderInfoMapper.selectByMap(paramMap);
    }

    @Override
    public AllOrderDetailInfoVO getAllOrderDetail(Long tableId, String openId) {
        List<OrderDetailInfoVO> cartList = Lists.newArrayList();
        List<OrderDetailInfoVO> orderList = Lists.newArrayList();

        List<OrderInfo> orderInfoList = this.getAllOrderByTableIdAndOpenId(tableId, openId);;
        if (CollectionUtils.isEmpty(orderInfoList)) {
            return AllOrderDetailInfoVO.builder().total(new BigDecimal(0)).build();
        }
        Long orderInfoId = orderInfoList.get(0).getId();
        BigDecimal total = orderInfoList.get(0).getTotal();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("order_info_id", orderInfoId);
        List<OrderDetailInfo> orderDetailInfoList = orderDetailInfoMapper.selectByMap(paramMap);
        orderDetailInfoList.forEach(orderDetailInfo -> {
            Long menuId = orderDetailInfo.getMenuInfoId();
            Integer status = orderDetailInfo.getStatus();
            MenuInfo menuInfo = menuInfoMapper.selectById(menuId);

            OrderDetailInfoVO.OrderDetailInfoVOBuilder orderDetailInfoVOBuilder = OrderDetailInfoVO.builder()
                    .orderDetailId(orderDetailInfo.getId()).status(orderDetailInfo.getStatus())
                    .pictureUrl(menuInfo.getPictureUrl()).title(menuInfo.getTitle()).price(menuInfo.getPrice()).num(orderDetailInfo.getNum());
            if (Objects.equals(status, OrderDetailInfoStatus.ToPlaceTheOrder.getCode())) {
                orderDetailInfoVOBuilder.selected(Boolean.FALSE);
                cartList.add(orderDetailInfoVOBuilder.build());
            } else {
                orderList.add(orderDetailInfoVOBuilder.build());
            }
        });

        return AllOrderDetailInfoVO.builder().cartList(cartList).orderList(orderList).orderId(orderInfoId).total(total).build();
    }

    @Override
    public void addOrderDetail(Long tableId, String openId, Long menuId) {
        List<OrderInfo> orderInfoList = this.getAllOrderByTableIdAndOpenId(tableId, openId);

        Long orderInfoId;
        if (CollectionUtils.isEmpty(orderInfoList)) {
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setTableId(tableId).setOpenId(openId).setTotal(new BigDecimal(0))
                    .setStatus(OrderInfoStatus.ToPlaceTheOrder.getCode()).setIsDelete(IsDelete.No.getCode());
            orderInfoMapper.insertReturnKey(orderInfo);
            orderInfoId = orderInfo.getId();
        } else {
            orderInfoId = orderInfoList.get(0).getId();
        }

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("order_info_id", orderInfoId);
        paramMap.put("menu_info_id", menuId);
        List<OrderDetailInfo> orderDetailInfoList = orderDetailInfoMapper.selectByMap(paramMap);
        OrderDetailInfo orderDetailInfo = new OrderDetailInfo();
        if (CollectionUtils.isEmpty(orderDetailInfoList)) {
            orderDetailInfo.setMenuInfoId(menuId).setStatus(OrderDetailInfoStatus.ToPlaceTheOrder.getCode())
                    .setOrderInfoId(orderInfoId).setNum(1);
            orderDetailInfoMapper.insert(orderDetailInfo);
        } else {
            Long orderDetailInfoId = orderDetailInfoList.get(0).getId();
            Integer sum = orderDetailInfoList.get(0).getNum();
            orderDetailInfo.setId(orderDetailInfoId).setNum(++sum);
            orderDetailInfoMapper.updateById(orderDetailInfo);
        }
    }

    @Override
    public void placeAnOrder(Long orderDetailId, Integer num) {
        OrderDetailInfo orderDetailInfo = orderDetailInfoMapper.selectById(orderDetailId);
        Long orderInfoId = orderDetailInfo.getOrderInfoId();

        Long menuId = orderDetailInfo.getMenuInfoId();
        MenuInfo menuInfo = menuInfoMapper.selectById(menuId);
        BigDecimal price = menuInfo.getPrice();
        BigDecimal total = new BigDecimal(num).multiply(price);

        if (Objects.equals(num, 0)) {
            orderDetailInfoMapper.deleteById(orderDetailId);
        } else {
            orderDetailInfo.setNum(num);
            orderDetailInfo.setStatus(OrderDetailInfoStatus.HaveOrder.getCode());
            orderDetailInfoMapper.updateById(orderDetailInfo);
        }

        Integer count = orderDetailInfoMapper.selectCount(new QueryWrapper<OrderDetailInfo>().eq("order_info_id",orderInfoId));
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setId(orderInfoId);
        if (Objects.equals(count, 0)) {
            orderInfo.setIsDelete(IsDelete.Yes.getCode());
        } else {
            orderInfo.setTotal(total);
            orderInfo.setStatus(OrderInfoStatus.ToForCooking.getCode());
        }

        orderInfoMapper.updateById(orderInfo);
    }

    @Override
    public void checkout(Long orderInfoId) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setId(orderInfoId).setStatus(OrderInfoStatus.CheckCompleted.getCode());
        orderInfoMapper.updateById(orderInfo);
    }

    @Override
    public void updateOrderStatus(Long orderInfoId, Integer status) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setId(orderInfoId).setStatus(status);
        orderInfoMapper.updateById(orderInfo);
    }

    @Override
    public void callService(Long tableId, String openId) {
        List<OrderInfo> orderInfoList = this.getAllOrderByTableIdAndOpenId(tableId, openId);
        orderInfoList.forEach(orderInfo -> {
            orderInfo.setFlag(Flag.Call.getCode());
            orderInfoMapper.updateById(orderInfo);
        });
    }

    @Override
    public void callServiceFinish(Long orderInfoId) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setId(orderInfoId).setFlag(Flag.Finish.getCode());
        orderInfoMapper.updateById(orderInfo);
    }

    @Override
    public void turnTable(Long orderInfoId, Long tableId) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setId(orderInfoId).setTableId(tableId);
        orderInfoMapper.updateById(orderInfo);
    }

    private List<OrderInfo> getAllOrderByTableIdAndOpenId(Long tableId, String openId) {
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("table_id", tableId).eq("open_id", openId)
                .eq("is_delete", IsDelete.No.getCode()).ne("status", OrderInfoStatus.CheckCompleted.getCode());

        return orderInfoMapper.selectList(queryWrapper);
    }
}