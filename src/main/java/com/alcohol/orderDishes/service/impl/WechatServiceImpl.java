package com.alcohol.orderDishes.service.impl;

import com.alcohol.orderDishes.service.WechatService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @Author: Lixinyu
 * @Date: Created in 11:01 2019-02-28
 * @Description:
 * @Modified By:
 */
@Service
public class WechatServiceImpl implements WechatService {

    private static final String URL = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";

    @Resource
    private RestTemplate restTemplate;

    @Override
    public JSONObject login(String appId, String appSecret, String code) {
        String url = String.format(URL, appId, appSecret, code);
        String jsonStr = restTemplate.getForEntity(url, String.class).getBody();
        return JSON.parseObject(jsonStr);
    }
}