package com.alcohol.orderDishes.service;


import com.alibaba.fastjson.JSONObject;

/**
 * @Author: Lixinyu
 * @Date: Created in 10:56 2019-02-28
 * @Description:
 * @Modified By:
 */
public interface WechatService {

    JSONObject login(String appId, String appSecret, String code);
}