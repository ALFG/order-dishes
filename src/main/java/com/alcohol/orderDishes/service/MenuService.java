package com.alcohol.orderDishes.service;

import com.alcohol.orderDishes.entity.MenuInfo;
import com.alcohol.orderDishes.vo.request.MenuInfoVO;

import java.util.List;

/**
 * @Author: Lixinyu
 * @Date: Created in 15:43 2019-02-26
 * @Description:
 * @Modified By:
 */
public interface MenuService {

    List<MenuInfo> getAllMenus(Integer type);
    MenuInfo getMenuDetail(Long id);
    void deleteMenu(Integer id);
    void addMenu(MenuInfoVO menuInfoVO);
    void editMenu(MenuInfoVO menuInfoVO);
}