package com.alcohol.orderDishes.service;

import com.alcohol.orderDishes.entity.OrderInfo;
import com.alcohol.orderDishes.vo.response.AllOrderDetailInfoVO;

import java.util.List;

/**
 * @Author: Lixinyu
 * @Date: Created in 11:12 2019-02-27
 * @Description:
 * @Modified By:
 */
public interface OrderService {

    List<OrderInfo> getAllOrder(Integer status);

    AllOrderDetailInfoVO getAllOrderDetail(Long tableId, String openId);

    void addOrderDetail(Long tableId, String openId, Long menuId);

    void placeAnOrder(Long orderDetailId, Integer num);

    void checkout(Long orderInfoId);

    void updateOrderStatus(Long orderInfoId, Integer status);

    void callService(Long tableId, String openId);

    void callServiceFinish(Long orderInfoId);

    void turnTable(Long orderInfoId, Long tableId);
}